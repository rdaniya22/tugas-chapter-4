
class Pilihan{
 
  constructor(element,name){
    this.element = element;
    this.name = name;
  }
  giveBorder(){ 
    this.element.classList.add('addBorder');
  }
  removeBorder(){  
    this.element.classList.remove('addBorder');
  }
  setElement(element){
    this.element = element;
  }
  setName(name){
    this.name = name;
  }
  getName(){
    return this.name;
  }
  getElement(){
    return this.element;
  }
}

class GamePlay{

  constructor(arrUser,arrComp,user){

    this.arrUser = arrUser;
    this.arrComp = arrComp;
    this.user = user;
    let randIndex = Math.floor(Math.random() * this.arrComp.length);
    this.compEl =this.arrComp[randIndex];
    this.comp = this.compEl.getAttribute('name');
    


  }

  disableBtnPlayer(){
    for (let i = 0; i < this.arrUser.length; i++) {
      this.arrUser[i].classList.add('pointerNone');
    }
  }
  enableBtnPlayer(){
    for (let i = 0; i < this.arrUser.length; i++) {
      this.arrUser[i].classList.remove('pointerNone');
    }
  }

  
 
  
  setPlayer(user){
    this.user = user;
  }

  getPlayer(){
    return this.user;
  }

  getComp(){
    return this.comp;
  }

  getCompEl(){

    return this.compEl;

  }
 
  playerChoice(){
    return 'User telah memilih ' + this.user;
  }
  compChoice(){
    return 'Computer telah memilih ' + this.comp;
  }
  determineWinner (){
    let result = "";
    if (this.user ==='rock'){
      if(this.comp ==='paper'){
        result="Computer WIN";
      }
      else if(this.comp ==='scissors'){
        result="Player 1 WIN";
      }else{
        result="DRAW";    
      }
      
    }
    
    else if (this.user ==='paper'){
      
      if(this.comp ==='rock'){
        result="Player 1 WIN";
      }
      else if(this.comp ==='scissors'){
        result="Computer WIN";
      }else{
        result="DRAW";
        
      }
    }
    else if (this.user ==='scissors'){
      if(this.comp ==='rock'){
        result="Computer WIN";
      }
      else if(this.comp ==='paper'){
        result="Player 1 WIN";
      }else{
        result="DRAW";
        
      }
      
    }
   
    return result;
  
  }

  showResut(){
    console.log(this.playerChoice());
    console.log(this.compChoice());
    console.log('Hasilnya: '+ this.determineWinner());
  }
}



const player = document.querySelectorAll("#player div");
const comp = document.querySelectorAll("#comp div");

let pilihanUser = new Pilihan();
let pilihanComp = new Pilihan();
let game1 = new GamePlay(player,comp);


let resetBtn = document.querySelector('.resetBtn');
let clickBtn = false;

//start the process
for (let i = 0; i < player.length; i++) {
  player[i].onclick = () =>{
    
    //set Player by clicking on 1 element in div #player
    let namePlayer = player[i].getAttribute('name');
    pilihanUser.setElement(player[i]);
    pilihanUser.setName(namePlayer);
    pilihanUser.giveBorder();
    game1.setPlayer(pilihanUser.getName())
   
    //get computer random choice
    pilihanComp.setElement(game1.getCompEl());
    pilihanComp.giveBorder();

    //display winner
    game1.showResut();

    let resultText = document.querySelector('#result');
    resultText.classList.add('showBtnResult');
    resultText.innerText = game1.determineWinner();

    //disable click
    
    clickBtn = true;
    if (clickBtn===true){

      game1.disableBtnPlayer();

    }

    //reset button
    resetBtn.onclick = () =>{
      
      pilihanUser.removeBorder();
      pilihanComp.removeBorder();
      clickBtn = false;
      game1.enableBtnPlayer();
      resultText.innerHTML='';
      resultText.classList.remove('showBtnResult');
      console.clear();
    } 
  };
}